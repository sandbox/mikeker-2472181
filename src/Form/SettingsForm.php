<?php

/**
 * @file
 * Contains Drupal\dropbox_app\Form\SettingsForm.
 */

namespace Drupal\dropbox_app\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\dropbox_app\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dropbox_app.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dropbox_app.settings');
    parent::buildForm($form, $form_state);

    $form['intro'] = [
      '#type' => 'markup',
      '#value' => '<p>' . $this->t('Enter the key and secret for your Dropbox App here. To create a Dropbox App, go to the <a href=":url">Dropbox developer site</a> and register an app.', [':url' => 'https://www.dropbox.com/developers/apps']),
    ];
    $form['key'] = [
      '#type' => 'textfield',
      '#default_value' => $config->get('key'),
      '#title' => $this->t('Dropbox App key'),
    ];
    $form['secret'] = [
      '#type' => 'textfield',
      '#default_value' => $config->get('secret'),
      '#title' => $this->t('Dropbox App secret'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save settings'),
    ];

    return ($form);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    if (empty($form_state->getValue('key'))) {
      $form_state->setErrorByName('key', 'The App key cannot be empty.');
    }
    if (empty($form_state->getValue('secret'))) {
      $form_state->setErrorByName('secret', 'The App secret cannot be empty.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('dropbox_app.settings')
      ->set('key', $form_state->getValue('key'))
      ->set('secret', $form_state->getValue('secret'))
      ->save();
  }

}
